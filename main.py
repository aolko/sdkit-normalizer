import argparse
import os
from enum import Enum
from PIL import Image
from rich.console import Console
from rich.progress import Progress
from rich.table import Table
import sdkit
from sdkit.generate import generate_images
from sdkit.models import load_model
from sdkit.utils import log, save_images


class Preset(Enum):
    ANIME_1 = "anime1"
    ANIME_2 = "anime2"
    FURRY_1 = "furry1"
    FURRY_2 = "furry2"


context = sdkit.Context()


def apply_preset(image_path: str, preset: Preset) -> None:
    with Image.open(image_path) as img:
        if preset == Preset.ANIME_1:
            # Apply preset for Anime 1
            pass
        elif preset == Preset.ANIME_2:
            # Apply preset for Anime 2
            pass
        elif preset == Preset.FURRY_1:
            # Apply preset for Furry 1
            pass
        elif preset == Preset.FURRY_2:
            # Apply preset for Furry 2
            pass
        img.save(image_path)


def apply_model(image_path: str, model_path: str) -> None:
    # set the path to the model file on the disk (.ckpt or .safetensors file)
    context.vram_usage_level = "low"
    context.model_paths["stable-diffusion"] = model_path
    load_model(context, "stable-diffusion")
    pass


def process_image(image_path: str, model_path: str, console: Console) -> None:
    console.print(f"Processing image: {image_path}")
    image_size = os.path.getsize(image_path)
    apply_model(image_path, model_path)
    #apply_preset(image_path, preset)
    with Image.open(image_path) as img:
        img_width, img_height = img.size
        new_img_width = img_width // 2
        new_img_height = img_height // 2
        images = generate_images(
            context,
            prompt="Photograph of an astronaut riding a horse",
            seed=0,
            width=new_img_width,
            height=new_img_height,
            init_image=img,
            num_inference_steps=50,
            guidance_scale=7.0,
        )
        save_images(images, dir_path=image_path)
    console.print(f"Processed image: {image_path}")
    console.print(f"  Original size: {img_width}x{img_height}")
    console.print(f"  New size: {new_img_width}x{new_img_height}")
    console.print(f"  File size: {os.path.getsize(image_path)} bytes\n")


def main():
    parser = argparse.ArgumentParser(description="Process images with presets using Rich package")
    parser.add_argument("path", type=str, metavar="IMAGE_PATH", help="Path to image or folder of images")
    parser.add_argument("--model", type=str, metavar="MODEL_PATH", required=True, help="Path to model file (.ckpt or .safetensors)")
    #parser.add_argument("--preset", type=Preset, choices=Preset, default=Preset.ANIME_1,
    #                    help="Preset to apply to image(s) (default: %(default)s)")
    args = parser.parse_args()

    path = os.path.abspath(args.path)
    console = Console()
    table = Table(title="Processed Images")
    table.add_column("Image")
    table.add_column("Original Size")
    table.add_column("New Size")
    table.add_column("File Size")

    if os.path.isfile(path):
        process_image(path, args.model, console)
        table.add_row(path, "", "")
    elif os.path.isdir(path):
        files = [os.path.join(path, f) for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        with Progress() as progress:
            task = progress.add_task("Processing images...", total=len(files))
            for file in files:
                process_image(file, args.model, console)
                table.add_row(file, "", "")
                progress.update(task, advance=1)

    console.print(table)


if __name__ == "__main__":
    main()
